package net.tngou.jtdb;


/**
* 排序列，这里只能对string和long数据进行排序
* @author tngou@tngou.net
* @date 2015年5月5日 下午4:01:41
*
 */
public class SortField {

	  private String name;  //字段名称
	  private Type  type;   //类型
	  private Order order;  //排序规则
	  

	public SortField(String name,Type  type) {
		
		  this.name = name;
		  this.type = type;
		  this.order=Order.ASC;
	  }
	 public SortField(String name,Type  type,Order order) {
		
		  this.name = name;
		  this.type = type;
		  this.order=order;
	  }
	  
	 public SortField(String name,Order order) {
			
		  this.name = name;
		  this.type = Type.Long;
		  this.order=order;
	  }
	  public enum Type{
		  //String，Long 主键 
			String,Long
		}
	  public enum Order{
		  //排序，升序，降序
			ASC,DESC
		}


	public String getName() {
		return name;
	}

	  public Order getOrder() {
			return order;
		}

		public void setOrder(Order order) {
			this.order = order;
		}

	public void setName(String name) {
		this.name = name;
	}



	public Type getType() {
		return type;
	}



	public void setType(Type type) {
		this.type = type;
	}
}
