package net.tngou.jtdb.netty;

import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import net.tngou.db.util.ResultSet;
import net.tngou.db.util.SerializationUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Netty Client 客户端接口
 * @author tngou@tngou.net
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {
	
	private ResultSet response =null;
	private static Log log = LogFactory.getLog(ClientHandler.class);
	

	 @Override
	 public void channelRead(ChannelHandlerContext ctx, Object msg){
			try {
				this.response= (ResultSet) SerializationUtils.deserialize((byte[]) msg);				
			} catch (IOException e) {
				log.error("");
				e.printStackTrace();
			}
	 }
	 
	public ResultSet getResponse() {
		return this.response;
	}
	 	
	public void clearResponse() {
		this.response=null;
	}
	 @Override
	 public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		 cause.printStackTrace();
		 ctx.close();
	 }
}