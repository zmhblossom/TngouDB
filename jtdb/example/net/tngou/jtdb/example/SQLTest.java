package net.tngou.jtdb.example;

import java.io.StringReader;
import java.util.Date;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;

public class SQLTest {

	public static void main(String[] args) {
		String sql="insert into topword(id:Key,title:Text,keywords:Text,description:Text,message:Text,topclass:String,img:String,type:String,url:String)  values('\"',?,?,?,?,?)";
		CCJSqlParserManager manager = new CCJSqlParserManager();
		
		try {
			manager.parse(new StringReader(sql));
		} catch (JSQLParserException e) {
			
			e.printStackTrace();
		}
		
		System.err.println(new Date().hashCode());

	}

}
