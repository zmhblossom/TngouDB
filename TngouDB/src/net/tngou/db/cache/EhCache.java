package net.tngou.db.cache;


import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.CacheManagerBuilder;
import org.ehcache.config.CacheConfigurationBuilder;

public class EhCache implements CacheEngine {

	
	private   String fullyQualifiedName;
	private  CacheManager cacheManager;
	private static CacheEngine  cacheEngine= null;
	public static CacheEngine getInstance(String fullyQualifiedName) {
		if(cacheEngine==null)
		{
			return new EhCache(fullyQualifiedName);
		}else
		{
			return cacheEngine;
		}
	}
	
	public EhCache(String fullyQualifiedName) {
		this.fullyQualifiedName=fullyQualifiedName;
		this.cacheManager=_getCacheManager();
	}

	@Override
	public void stop() {
		cacheManager.close(); 
		cacheManager=null;
		map.put(fullyQualifiedName, null);
		cacheEngine= null;
		
	}

	@Override
	public void add(String key, Object value) {
		Cache<String, Object> myCache =cacheManager. getCache(fullyQualifiedName, String.class, Object.class);
		myCache.put(key, value);
//		map.put(fullyQualifiedName, cacheManager);
	}

	@Override
	public Object get(String key) {
		Cache<String, Object> myCache =cacheManager. getCache(fullyQualifiedName, String.class, Object.class);
		
		return myCache.get(key);
	}

	

	@Override
	public void remove(String key) {
		Cache<String, Object> myCache =cacheManager. getCache(fullyQualifiedName, String.class, Object.class);
		myCache.remove(key);
//		map.put(fullyQualifiedName, cacheManager);
		
	}

	@Override
	public void remove() {
		Cache<String, Object> myCache =cacheManager. getCache(fullyQualifiedName, String.class, Object.class);
		myCache.clear();
//		cacheManager.removeCache(fullyQualifiedName);
//		cacheManager.close();
		
	}
	
	
	
	private CacheManager _getCacheManager() {
		
		CacheManager cacheManager=(CacheManager) map.get(fullyQualifiedName);
		if(cacheManager==null)
		{
			cacheManager
		    = CacheManagerBuilder.newCacheManagerBuilder() 
		    .withCache(fullyQualifiedName,
		        CacheConfigurationBuilder.newCacheConfigurationBuilder()
		            .buildConfig(String.class, Object.class)) 
		    		.build(true);  //出现锁死状态，更新了 echache 的版本，添加了true
			map.put(fullyQualifiedName, cacheManager);
		}
		
		return cacheManager;
	}


}
